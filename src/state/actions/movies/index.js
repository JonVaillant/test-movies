export const STORE_MOVIES = 'STORE_MOVIES'

export const storeMovies = movies => ({
  type: STORE_MOVIES,
  movies
})