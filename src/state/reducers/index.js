import { combineReducers } from 'redux';

import moviesState from './movies';

const appState = combineReducers({
  moviesState
});

export default appState;