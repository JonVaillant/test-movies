import { combineReducers } from 'redux';

import { STORE_MOVIES } from '../../actions/movies';

const movieData = (state = {
  movies: []
}, action) => {
  const newState = Object.assign({}, state)

  switch (action.type) {
    case STORE_MOVIES:
      newState.movies = action.movies

      return newState;

    default:
      return state;
  }
}

const moviesState = combineReducers({
  movieData
});

export default moviesState;