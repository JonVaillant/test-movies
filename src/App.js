import React, { Component } from 'react';

import MovieButton from './components/movie-button';
import MovieListing from './components/movie-listing';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MovieButton
          store={this.props.store}
        />
        <MovieListing />
      </div>
    );
  }
}

export default App;
