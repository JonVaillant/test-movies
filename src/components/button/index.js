import React from 'react'

import iconSync from '../../statics/sync.svg'

const loadingIcon = (
  <img
    className="icon--loading"
    src={iconSync}
  />
)

const Button = props => (
  <button
    className={props.className}
    onClick={props.onClick}
  >
    {props.content}
    {props.loading && loadingIcon}
  </button>
)

export default Button