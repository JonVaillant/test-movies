import React from 'react'

import Button from '../button'

import {storeMovies} from '../../state/actions/movies'

const srcMovies = 'https://facebook.github.io/react-native/movies.json'

const headersMovies = {
  credentials: 'omit'
}

class MovieButton extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      error: false,
      loading: false
    }
  }

  fetchMovies = () => fetch(srcMovies, headersMovies)
    .then(response => response.json())
    .then(responseJson => {
      this.props.store.dispatch(
        storeMovies(responseJson.movies)
      )
    })
    .then(() => {
      this.setState({
        loading: false
      })
    })
    .catch(error => {
      console.error(error);

      this.setState({
        error: true,
        loading: false
      })
    })

  getMovies = () => {
    this.setState({
      error: false,
      loading: true
    })

    this.fetchMovies()
  }

  render() {
    const {
      error,
      loading
    } = this.state

    let buttonText = !error ? 'Load Movies' : 'Try Loading Movies Again'

    return (
      <Button
        className="movies--action"
        content={buttonText}
        loading={loading}
        onClick={this.getMovies}
      />
    )
  }
}

export default MovieButton