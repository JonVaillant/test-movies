import React from 'react'

import {connect} from 'react-redux'
import moviesState from '../../state/reducers/movies';

const MoviesListing = props => {
  const {
    movies
  } = props

  if (!movies) {
    return <div>No movies loaded</div>
  }

  const listing = movies.map((movie, mI) => (
    <li key={mI}>{movie.title}</li>
  ))

  return (
    <ul>{listing}</ul>
  )
}

const mapStateToProps = state => {
  return {
    movies: state.moviesState.movieData.movies
  };
};

export default connect(mapStateToProps)(MoviesListing)