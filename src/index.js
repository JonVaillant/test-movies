import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import state from './state'

import App from './App'
import './index.css'

import * as serviceWorker from './serviceWorker'

const store = state()

const root = (
  <Provider store={store}>
    <App store={store} />
  </Provider>
)

ReactDOM.render(root, document.getElementById('root'));

serviceWorker.unregister();
