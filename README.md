# Movie App

  - create a new react-based app
  - using this snippets as an example:

```js
  function getMoviesFromApiAsync() {
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson.movies;
      })
      .catch((error) => {
        console.error(error);
      });
  }
```

  - get movie titles from api and display it
  - add a button to fetch the movie titles with a loading state
  - replace react state with redux